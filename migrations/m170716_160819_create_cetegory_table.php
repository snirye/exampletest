<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cetegory`.
 */
class m170716_160819_create_cetegory_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cetegory', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cetegory');
    }
}
